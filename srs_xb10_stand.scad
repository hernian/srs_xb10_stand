$fn = 120;


difference(){
    union(){
        difference(){
            translate([0, -41, 0]){
                roundedcube(82, 82, 35, 3);
            }
            translate([3, -38, -1]){
                roundedcube(76, 76, 33, 3);
            }
        }
        intersection(){
            translate([0, 0, 36]){
                rotate([0, 60, 0]){
                    difference(){
                        translate([0, 0, 0]){
                            cylinder(r=41, h=75);
                        }
                        translate([0, 0, 3]){
                            cylinder(r=38, h=80);
                        }
                    }
                }
            }
            translate([0, -41, 0]){
                roundedcube(82, 82, 35, 3);
            }
        }
    }
    translate([0, 0, 36]){
        rotate([0, 60, 0]){
            translate([0, 0, 3]){
                cylinder(r=38, h=80);
            }
        }
    }
}

/*
difference(){
    union(){
        translate([0, 41, 0]){
            cylinder(r=10, h=1);
        }
        translate([82, 41, 0]){
            cylinder(r=10, h=1);
        }
        translate([82, -41, 0]){
            cylinder(r=10, h=1);
        }
        translate([0, -41, 0]){
            cylinder(r=10, h=1);
        }
    }
    translate([0, -41, -1]){
        roundedcube(82, 82, 35, 3);
    }
}
*/



module roundedcube(xdim, ydim, zdim, rdim){
    hull(){
        translate([rdim,rdim,0])cylinder(h=zdim,r=rdim);
        translate([xdim-rdim,rdim,0])cylinder(h=zdim,r=rdim);

        translate([rdim,ydim-rdim,0])cylinder(h=zdim,r=rdim);
        translate([xdim-rdim,ydim-rdim,0])cylinder(h=zdim,r=rdim);
    }
}
